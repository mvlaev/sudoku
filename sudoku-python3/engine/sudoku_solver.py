from random  import choice

from engine.sudoku_board  import Board

class SudokuSolve:
	def __init__(self, board: Board, *, dim = 9):
		self.board  = board
		self.blanks = self.board.get_blanks()
		self.dim    = dim
		self.backtrack_lenght = []
#		self.recursion_count = -1
#		print(len(self.blanks))

	def manage_backtrack(self, backtrack):
		###Choose a value for the point
		new_choice = choice(available_choices)
		available_choices.remove(new_choice)
		backtrack.append((point, (new_choice, available_choices)))
		return backtrack
		
	def solve_trivial(self, board):
		blanks = board.get_blanks()
		
		###This works!
		stop = False
		while not stop:
			stop = True
			
			for blank in blanks:
#				print(blank)
				line, row = blank
				values    = board.get_available_values(line, row)
				
				if len(values) == 1:
					board.set_item(values[0], line, row)
#					print(values[0], ' @ ', line, row)
					blanks.remove(blank)
					stop = False
				elif len(values) == 0:
#					print(f'no solution', line, row)
#					print(board)
					return 'UNSOLVABLE'
		return 'DONE'
	
	def make_choice_from_blanks(self, board, backtrack = None):
		'''
		
		backtrack - a list of the form [(point1, (last_choice, [available_choices]))]
		'''
		if backtrack == None:
			backtrack = []
			
		blanks  = board.get_blanks()
		
		if not blanks:
			return 'SOLVED'#{}
		
		choices = {blank: board.get_available_values(blank[0], blank[1]) for blank in blanks}
		
		###Choose the point with less possible choices:
		point = sorted(choices, key=lambda i: len(choices[i]))[0]
		available_choices = choices[point]
		############################################################
		###Choose a value for the point
		new_choice = choice(available_choices)
		available_choices.remove(new_choice)
		backtrack.append((point, (new_choice, available_choices)))
		
#		print(point)
#		[print(i) for i in choices.items()]
		
		return backtrack
		
	def make_choice_from_backtrack(self, board, backtrack = None):
		'''
		
		backtrack - a list of the form [
										(point1, (last_choice, [available_choices])),
										(point2, (last_choice, [available_choices])),
										.............]
		'''
#		if backtrack == None:
#			return 'UNSOLVEABLE'
		
		###If a backtrack exists:
		last_backtrack_item = backtrack.pop()
		point, (last_choice, available_choices) = last_backtrack_item
		###################################
		
		if available_choices:
			new_choice = choice(available_choices)
			available_choices.remove(new_choice)
			backtrack.append((point, (new_choice, available_choices)))
		
		return backtrack
		###################################
		
#		t = [True for i in l if len(l[i]) > 0]
#		print(len(l) == len(t))
	
	def solve(self):
		###Initialize
		last_board = []
		
		backtrack = []
		
		current_board = Board(structure = self.board.get_board())
		while True:
			#####Some backtrack info
			self.backtrack_lenght.append(len(backtrack))
			#################################################
			
			###Save state
			last_board.append(current_board)
			###Create new board
			current_board = Board(structure = current_board.get_board())
			###Solve trivial
			solve_trivial_result = self.solve_trivial(current_board)
#			print(solve_trivial_result, len(backtrack))
			
			###Is it unsolveable?
			if solve_trivial_result == 'UNSOLVABLE':
				###YES
				###If there's backtrack reverse and try again
				if backtrack:
					###reverse
					current_board = last_board.pop()
					
					backtrack = self.make_choice_from_backtrack(current_board, backtrack)
					if not backtrack:
						return 'UNSOLVABLE'
					
					last_backtrack_item = backtrack.pop()
					point, (last_choice, available_choices) = last_backtrack_item
					current_board.set_item(last_choice, point[0], point[1])
					backtrack.append(last_backtrack_item)
					
						
				else:
				###If no backtrack return 'UNSOLVABLE'
					return 'UNSOLVABLE'
						
			###NO
			elif solve_trivial_result == 'DONE':
				
				###Choose a non trivial
				
				current_board = Board(structure = current_board.get_board())
				#current_board = Board(structure = deepcopy(current_board.get_board()))
				
				backtrack = self.make_choice_from_blanks(current_board, backtrack)
				
				if backtrack == 'SOLVED':
#						print('SOOOOOOOOOOOOOOOOLVED!')
					self.board = current_board
					return current_board
					
				last_backtrack_item = backtrack.pop()
				point, (last_choice, available_choices) = last_backtrack_item
				current_board.set_item(last_choice, point[0], point[1])
				backtrack.append(last_backtrack_item)
				###Continue
		
		
	

if __name__ == '__main__':
	###Needed for debugging
	from timeit import repeat
	from copy import deepcopy
	def go():
		board = Board()
		board._Board__structure = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,0,2,8],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
		]
		
		print(board)
		print()
		solver = SudokuSolve(board)
		b = solver.solve()
		return b
	
	def t():
		time = repeat(go, repeat=1000, number=1)
		
		
		print(f'''\
sum   = {sum(time)}
worst = {max(time)}
best  = {min(time)}
avg   = {sum(time)/len(time)}
''')

	t()
#	print(go())
