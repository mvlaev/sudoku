from copy             import deepcopy
from multiprocessing  import Pool, cpu_count

from engine.sudoku_board     import Board
from engine.sudoku_generator import SudokuGen
from engine.sudoku_solver    import SudokuSolve
from engine.sudoku_unsolver  import SudokuUnsolve

class SudokuOps:
	def __init__(self):
		cores      = cpu_count()
		self.cores = cores if cores < 4 else 4
	
	def __return_board(self, structure):
		t = type(structure).__name__
		if t == 'list':
			board = Board(structure = structure)
		elif t == 'Board':
			board = structure
		else:
			raise TypeError(f'structure must be of type list or Board, instead type {t} passed!')
		return board
	
	def board_is_ok(self, board):
		if board.check_board():
			return True
		return False
	
	def generate_async(self):
		gen = SudokuGen()
		
		with Pool(self.cores) as p:
			for result in p.imap_unordered(gen.generate_solution, [()]*self.cores):
	#			print(result)
				return result
	
	
	def unsolve_async(self, board, blanks_count):
		unsolver = SudokuUnsolve(board)
		
		with Pool(self.cores) as p:
			for result, backtrack_lenght in p.imap_unordered(unsolver.unsolve, ((blanks_count,))*self.cores):
				print('backtrack lenght', backtrack_lenght)
				return result
	
	def solve_existing(self, structure):
		#TODO: CHECK THE STRUCTURE
		board = self.__return_board(structure)
		if not self.board_is_ok(board):
			return 'Bad structure!'
		
		solver = SudokuSolve(board)
		result = solver.solve()
		
		#RESULT MAY BE STRING
		if type(result).__name__ == 'Board':
			return result
		raise Exception('puzzle is UNSOLVEABLE')
	
	def get_puzzle_board(self, blanks_count):
		if not 0 < blanks_count < 81:
			raise Exception(f'blanks_count must be between 0 and 81, instead {blanks_count} given')
		
		board = self.generate_async()
		print(f'\ngenerated solution\n{"-"*10}')
		print(board)
		print(f'{"-"*10}')
		board = self.unsolve_async(board, blanks_count)
		print(f'generated puzzle\n{"-"*10}')
		print(board)
		print(f'{"-"*10}')
		return board
	
	
	def get_puzzle_as_list(self, blanks_count):
		return deepcopy(self.get_puzzle_board(blanks_count).get_board())
	
	def get_puzzle_as_dict(self, blanks_count):
		puzzle_board = self.get_puzzle_board(blanks_count)
		puzzle_as_dict = puzzle_board.get_board_as_dict()
		return puzzle_as_dict
	

if __name__ == '__main__':
	from time import monotonic
	

####################################
#board = Board(structure = [
#										[5,3,0,0,7,0,0,0,0],
#										[6,0,0,1,9,5,0,0,0],
#										[0,9,8,0,0,0,0,6,0],
#										[8,0,0,0,6,0,0,0,3],
#										[4,0,0,8,0,3,0,0,1],
#										[7,0,0,0,2,0,0,0,6],
#										[0,6,0,0,0,0,0,2,8],
#										[0,0,0,4,1,9,0,0,5],
#										[0,0,0,0,8,0,0,7,9]
#										]
#										)


###	
#	board = Board(structure = [
#	[9, 5, 1, 2, 4, 6, 7, 8, 3],
#	[8, 2, 7, 9, 5, 3, 6, 1, 4],
#	[6, 4, 3, 8, 7, 1, 9, 5, 2],
#	[2, 3, 8, 5, 9, 4, 1, 7, 6],
#	[1, 6, 9, 3, 8, 7, 4, 2, 5],
#	[4, 7, 5, 6, 1, 2, 8, 3, 9],
#	[5, 9, 4, 1, 3, 8, 2, 6, 7],
#	[7, 1, 6, 4, 2, 5, 3, 9, 8],
#	[3, 8, 2, 7, 6, 9, 5, 4, 1],
#	]
#	)
#	for i in range(300): print(unsolve_async(board))
################################################################


	#unsolver_async()
	
######Timing test################################################
	def process_timer():
		time = []
		for i in range(100):
			game = Sudoku()
			current = monotonic()
			game.get_puzzle_board(52)
			current = monotonic() - current
			time.append(current)
		
		print(f'''\
sum   = {sum(time)}
worst = {max(time)}
best  = {min(time)}
avg   = {sum(time)/len(time)}
''')
	
	def solve_board():
		
		time = []
		for i in range(1000):
			current = monotonic()
#			solve_async(board)
			generate_async()
			current = monotonic() - current
			time.append(current)
		
		print(f'''\
sum   = {sum(time)}
worst = {max(time)}
best  = {min(time)}
avg   = {sum(time)/len(time)}
''')
	
	process_timer()
#######END Timing test##########################################################

#	time = repeat(process_async, repeat = 30, number = 1)
#	print(f'''\
#sum   = {sum(time)}
#worst = {max(time)}
#best  = {min(time)}
#avg   = {sum(time)/len(time)}
#''')
###############################################################################

	#b = go()
	#solver = SudokuSolve(b)

	#print(solver.solve())
	#	print(test[0])
	#	print('solved\n', str(board))




