from random  import choice

from engine.sudoku_board import Board

class SudokuGen:
	def __init__(self, * , dim = 9):
		self.board = Board()
		self.dim   = dim
#		self.counts = dict([(i,9) for i in range(1,10)])
#		self.counts = []
		
	def board_fill(self):
		for i in range(self.dim):
			for j in range(self.dim):
				available_numbers = self.board.get_available_values(y = i, x = j)
				
				try:
#					random_num = sample(available_numbers, 1, counts = [self.counts[number] for number in available_numbers])[0]
					###Try getting a random number out of "available_numbers":
					random_num = choice(available_numbers)
				except:
					###Cannot get random number
					#NO SOLLUTION!!!!!!
					return False
				
				###Set the chosen number
				self.board.set_item(random_num, i, j)
				#self.counts[random_num] -= 1
		
		###Board filled successfuly
		return True
	
	
	def generate_solution(self, *dummy):
#		counter = 0
		
		while not self.board_fill():
			self.board.reset()
		return self.board
#			counter += 1
#		
#		print('generated in {} tries'.format(counter))
#		gen.counts.append(counter)
#		now = monotonic() - now
#		print(f'generated in {now} sec.\n{counter} tries.\n{now/counter:.4f} sec. per try')
#		print('from generate_solution:\n\n', self.board)



if __name__ == '__main__':
	def go():
		board = Board()
		gen = SudokuGen(board)
		
		gen.generate_solution()
		
		print('from go\n{}'.format(board))
		
	from timeit  import timeit, repeat
	time = repeat(go, repeat=100, number=1)
	print(f'''\
sum   = {sum(time)}
worst = {max(time)}
best  = {min(time)}
avg   = {sum(time)/len(time)}
''')

#	board = Board()
#	gen = SudokuGen(board)
#	for i in range(100):
#		gen.generate_solution()

#	print(f'''generated 100 solutions
#worst = {max(gen.counts)}
#best  = {min(gen.counts)}
#avg   = {sum(gen.counts)/len(gen.counts)}
#	''')
