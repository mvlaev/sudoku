import PySimpleGUI as sg

from multiprocessing import freeze_support, set_start_method

from GUI.gui_events import SudokuEvents

def gui():
	gui = SudokuEvents()
	gui.loop()

if __name__ == '__main__':
	freeze_support()
	set_start_method('spawn')
	
	gui()

