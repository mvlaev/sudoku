import PySimpleGUI as sg

from time import sleep

import GUI.popups as popups
from GUI.timer import Timer
from GUI.gui_layouts import SudokuLayouts
from engine.sudoku_ops import SudokuOps

class SudokuLogic:
	def __init__(self):
		self.relative_location = (-700/2,-500/2)
		self.current_focus     = (4,4)
		self.new_focus         = (4,4)
		self.colors_on         = False
		
		self.layouts           = SudokuLayouts()
		self.popups            = popups
		#
		self.engine            = SudokuOps()
		self.board             = None
		self.blanks            = []
		#
		self.window            = None
		self.timer             = None
		
		self.init_window()
		
		
		
	def init_window(self):
#		splash = sg.Window('', [[sg.Text('hello')]], size=(200,200),keep_on_top=True, no_titlebar=True, background_color='brown', finalize=True)
		
		self.window  = sg.Window('Sudoku', self.layouts.top_layout, alpha_channel=0, resizable=False, return_keyboard_events = True, relative_location = self.relative_location, enable_close_attempted_event=True, finalize=True)
		self.timer = Timer(self.window['-TIMER-'])
		
		################################update the board values##################################################
		welcome = [list('*'*9),
				   list('!WELCOME!'),
				   list('*'*9),
				   list('**Let\'s**'),
				   list('*'*9),
				   list('***play**'),
				   list('*'*9),
				   list('*SUDOKU!*'),
				   list('*'*9)
				]
		wlcm_dict = {(i,j):welcome[i][j] for i in range(9) for j in range(9)}
		self.window.fill(wlcm_dict)
		##########
		self.window['-BOARD-'].update(visible=True)
		###################################################################################
		
		###update the tools parts#############################
		self.window['-TOOLS-'].update(visible=True)
		self.timer.reset()
		self.window['-START-'].update(disabled=True)
		######################################################
		
		###refresh and show the window############
#		self.window.refresh()
#		self.hide_board()
#		self.window['-BOARD-'].set_size(size=self.layouts.board_size)
		self.hide_board()
		for i in range(100):
			sleep(0.01)
			self.window.set_alpha(i/100)
			self.window.refresh()
		##########################################
		
		###Finish
#		splash.close()
		sleep(0.5)
		self.show_board()
		#########################
	
	def you_won(self):
		if not len(self.board.get_blanks()):
			self.start_stop()
			self.show_board()
			self.window['-START-'].update(disabled=True, button_color='lightgrey')
			time = self.timer.timer.DisplayText
			
			self.popups.you_won_popup(time)
		
	
	def update_all(self):
		if not self.timer.running:
			return
		
		self.update_colors()
		self.update_bar()
		self.update_timer()
		self.you_won()
	
	def update_timer(self):
		self.timer.refresh()
	
	def update_bar(self):
		blanks_count = len(self.board.get_blanks())
		
		self.window['PROGRESS'].update(current_count=blanks_count)
		self.window['BLANKS'].update(value=self.layouts.blanks_left_format.format(blanks_count))
		
	def update_focus(self):
		self.current_focus = self.new_focus
	
	def update_colors(self):
		self.decolorize()
		self.colorize()
		self.update_focus()
		
	
########BOARD OPS#############################################################################
	def __board_animation(self, show=True):
		smooth = 10
		duration = 0.1
		sleep_time = duration/smooth
		if show:
			r = (smooth+1,)
		else:
			r = (smooth, -1, -1)
			
		for i in range(*r):
			sleep(sleep_time)
			self.window['-BOARD-'].set_size(size=(self.layouts.board_size[0],self.layouts.board_size[1]*i/smooth))
			self.window.refresh()
	
	def show_board(self):
		self.__board_animation(True)
	
	def hide_board(self):
		self.__board_animation(False)
###################################################################################
	
###############Color ops#########################################
	def colorize(self):
		###Color the blanks please!
		for el in self.blanks:
			self.window[el].update(text_color='black', background_color='lightyellow')
			
		if self.colors_on:
			for line in range(9):
				self.window[line, self.new_focus[1]].update(text_color='black', background_color='grey')
				
			for row in range(9):
				self.window[self.new_focus[0], row].update(text_color='black', background_color='grey')
			
			metadata = self.window[self.new_focus].metadata
			[[el.update(text_color='black', background_color='grey') for el in line] for line in self.layouts.layout_dict[metadata]]
		
		
		
		if self.new_focus in self.blanks:
			self.window[self.new_focus].update(text_color='black', background_color='lightgreen')
		else:
			self.window[self.new_focus].update(text_color='black', background_color='lightblue')
	

	def decolorize(self, force=False):
		if self.colors_on:
			for line in range(9):
				self.window[line,self.current_focus[1]].update(text_color='white', background_color = self.window.BackgroundColor)
				
			for row in range(9):
				self.window[self.current_focus[0], row].update(text_color='white', background_color = self.window.BackgroundColor)
			
			metadata     = self.window[self.current_focus].metadata
			new_metadata = self.window[self.new_focus].metadata
			
			if not metadata == new_metadata or force:
				[[el.update(text_color='white', background_color=self.window.BackgroundColor) for el in line] for line in self.layouts.layout_dict[metadata]]
		
		self.window[self.current_focus].update(text_color='white', background_color=self.window.BackgroundColor)
		
		
#############################################################################
		
#	def show_window(self):
#		self.window['-BOARD-'].update(visible=True)
#		self.window['-TOOLS-'].update(visible=True)
#		self.window['-START-'].update(disabled=True)
#		self.window.refresh()
#		
#		self.hide_board()
#		for i in range(100):
#			sleep(0.01)
#			self.window.set_alpha(i/100)
#			self.window.refresh()
#		
#		self.splash.close()
#		sleep(0.5)
#		self.show_board()
		
	def close_window(self):
		self.timer.stop()
		self.hide_board()
		self.window.close()

########Button CALLBACKS
	def start_stop(self):
		button = self.window['-START-']
		
		if self.timer.running:
		###Game should pause
			self.timer.stop()
			self.hide_board()
			
			button.update(text='START', button_color=self.window.ButtonColor)
			
		else:
		###Game continues
			self.show_board()
			self.timer.start()
			
			button.update(text='PAUSE', button_color='red')
		#self.window.refresh()
	
	def new_board(self):
		blanks_count = self.popups.blanks_popup()
		
		if blanks_count == None:
			###Abort
			return
		
		if self.timer.running:
			self.start_stop()
		
		self.timer.reset()
		
		###Engine calls:
		self.board  = self.engine.get_puzzle_board(blanks_count)
		self.blanks = self.board.get_blanks()
		fill_dict   = self.board.get_board_as_dict()
		#############
		self.window.fill(fill_dict)
		
		###Interface init
		self.window['-START-'].update(disabled=False, button_color=self.window.ButtonColor)
		self.window['PROGRESS'].update(max=blanks_count, current_count=blanks_count)
		self.window['BLANKS'].update(value=self.layouts.blanks_left_format.format(blanks_count))
		##################
		
		self.start_stop()
	
	def colors_on_off(self):
		if self.colors_on:
			self.decolorize(force=True)
			self.colors_on = False
			self.window['-COLORS-'].update(text='Turn Colors ON',button_color=('blue','red'))
		else:
			self.colors_on = True
			self.window['-COLORS-'].update(text='Turn Colors OFF',button_color=('red', 'blue'))
	
############Focus control#################################################################
	def mouse_click(self, new:tuple):
		if self.timer.running:
			self.new_focus = new
	
	def key_up(self):
		if self.timer.running:
			self.new_focus = ((self.current_focus[0] - 1) % 9, self.current_focus[1])
	
	def key_down(self):
		if self.timer.running:
			self.new_focus = ((self.current_focus[0] + 1) % 9, self.current_focus[1])
	
	def key_left(self):
		if self.timer.running:
			self.new_focus = (self.current_focus[0], (self.current_focus[1] - 1) % 9)
	
	def key_right(self):
		if self.timer.running:
			self.new_focus = (self.current_focus[0], (self.current_focus[1] + 1) % 9)

##############################################################################################

########Set value####################################################
	def delete_value(self):
		###Are we playing?
		if not self.timer.running:
			###No. Do nothing.
			return
		###We are playing!
		######Is it a blank?
		if not self.current_focus in self.blanks:
			######It's NOT a blank. Do nothing.
			return
		######It's a blank! Delete it from self.board and from the GUI!
		self.board.set_item(0, *self.current_focus) 	 #deleted from the board
		self.window[self.current_focus].update(value='') #deleted from GUI
		
#		###Update the progress bar........
#		self.update_bar()
	
	def set_value(self,value:str):
		###Are we playing?
		if not self.timer.running:
			###No. Do nothing.
			return
		###We are playing!
		######Is it a blank?
		if not self.current_focus in self.blanks:
			######It's NOT a blank. Do nothing.
			return
		######It's a blank! Check if it is OK to store it at the current_focus?
		value = int(value)
		if not self.board.check_number(value, *self.current_focus):
			###OH NO! IT'S WROOONG!!!!
			self.popups.oh_no_popup(value)
			return
		###Woo hoo it's OK, write it!
		self.board.set_item(value, *self.current_focus)	    #stored in the board
		self.window[self.current_focus].update(value=value) #showed in the GUI
		
#		###Update the progress bar........
#		self.update_bar()
	
######################################################################################################################

if __name__ == '__main__':
	gl = SudokuLogic()
	while True:
		event, values = gl.window.read()
		
		if event in (sg.WIN_CLOSED, sg.WINDOW_CLOSE_ATTEMPTED_EVENT):
			gl.close_window()
			break
		
		if event == '-COLORS-':
			gl.colorize()
		
