import PySimpleGUI as sg

from time import sleep

from GUI.gui_layouts import SudokuLayouts
from GUI.gui_logic   import SudokuLogic

class SudokuEvents:
	def __init__(self):
		self.logic  = SudokuLogic()
		self.window = self.logic.window
		
	
	def loop(self):
		
		
		while True:
			self.logic.update_all()
			
			event, values = self.window.read(timeout=1000)
			print(event)
			
			if event in (sg.WIN_CLOSED, sg.WINDOW_CLOSE_ATTEMPTED_EVENT, '-CLOSE-'):
				self.logic.close_window()
				break
			
			
			###Cases
			event_type = type(event).__name__
			
			if event_type == 'tuple':
						###clicked with mouse
						self.logic.mouse_click(event)
			elif event_type == 'str':
				stripped = event.split(':')[0]
				
				if stripped in (map(str, list(range(1, self.logic.layouts.dim + 1)))):
					###Save the number
					self.logic.set_value(stripped)
				
				if stripped == '__TIMEOUT__':
					self.logic.update_timer()
				elif stripped == '-START-':
					self.logic.start_stop()
				elif stripped == '-NEW-':
					self.logic.new_board()
					#self.logic.start_stop()
				elif stripped == '-COLORS-':# if self.logic.timer.running:
					self.logic.colors_on_off()
				elif stripped == 'Up':
					self.logic.key_up()
				elif stripped == 'Down':
					self.logic.key_down()
				elif stripped == 'Left':
					self.logic.key_left()
				elif stripped == 'Right':
					self.logic.key_right()
				elif stripped in ('Delete','BackSpace'):
					self.logic.delete_value()
				elif stripped == 'c':
					self.logic.window['-COLORS-'].click()
				elif stripped == 'n':
					self.logic.window['-NEW-'].click()
				elif stripped == 's':
					self.logic.window['-START-'].click()
				

