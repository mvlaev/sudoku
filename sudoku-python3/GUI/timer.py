from time import monotonic, sleep

class Timer:
	def __init__(self, timer_object):
		self.timer = timer_object
		
		self.start_time   = 0
		self.elapsed_time = 0
		self.running      = False
		self.timer_format = '{:02d}:{:02d}:{:02d}'
		
	
	def refresh(self):
		if not self.running:
			return
		
		value = self.elapsed_time + monotonic() - self.start_time
		value = round(value)
		
		self.timer.update(value=self.timer_format.format(value // 3600, (value % 3600) // 60, (value % 3600) % 60))
		
	def start(self):
		self.start_time = monotonic()
		self.running = True
	
	def stop(self):
		self.elapsed_time += monotonic() - self.start_time
		self.running = False
	
	def reset(self):
		self.timer.update(value=self.timer_format.format(0,0,0))
		
		self.running = False
		self.start_time = 0
		self.elapsed_time = 0
		
		
		
