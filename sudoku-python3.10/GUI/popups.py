import PySimpleGUI as sg

def blanks_popup(value=31):
		background_color = 'brown'
		text_color       = 'black'
		
		layout = [[sg.T('How many blanks do you want?', background_color=background_color)],
		  [sg.Input(default_text=value, key='-VALUE-', size=3, font=('',50), justification='c', enable_events=True, expand_x=False)],
		  [sg.Button('OK', key='Ok', expand_x=True, bind_return_key=True, button_color=(text_color, background_color), mouseover_colors=background_color),
		   sg.Button('Cancel', key='Cancel', expand_x=True, bind_return_key=True, button_color=(text_color, background_color), mouseover_colors=background_color)
		   		]
			]
			
		window = sg.Window('Set Value', layout, element_justification='center', no_titlebar=True, modal=True, keep_on_top=True, background_color=background_color, disable_close=True, finalize=True)
		window['-VALUE-'].update(select=True)
		window['-VALUE-'].set_focus()
		
		while True:
			event, values = window.read()
			if event in ('Cancel', sg.WIN_CLOSED):
				chars = None
				break
			if event == 'Ok':
				print(event, values)
				
				chars = values['-VALUE-']
				print(chars.isnumeric())
				if (not chars.isnumeric()) or int(chars) > 81:
					window['-VALUE-'].update(value = value, select=True)
					window['-VALUE-'].set_focus()
					continue
				
				chars = int(chars)
				break
		
		window.close()
		return chars
		
def you_won_popup(time):
		background_color = 'brown'
		text_color       = 'black'
		you_won_txt      = f'Congratulations!\n\nYou solved the puzzle!\nYour time is:\n{time}'
		layout = [
		            [sg.T(you_won_txt, background_color=background_color, justification='center', font=('', 30))],
		            [sg.Button('OK', key='Ok', expand_x=True, bind_return_key=True, button_color=(text_color, background_color), mouseover_colors=background_color)]
				 ]
			
		window = sg.Window('YOU WON!', layout, element_justification='center', no_titlebar=True, modal=True, keep_on_top=True, background_color=background_color, disable_close=False, finalize=True)
		window['Ok'].set_focus()
		while True:
			event, values = window.read()
			
			if event in (sg.WIN_CLOSED, 'Ok'):
				break
		window.close()

def oh_no_popup(n):
		background_color = 'brown'
		text_color       = 'black'
		you_won_txt      = f'Oh nononono!\n\nCan\'t do that!\nWrong number:\n{n}'
		layout = [
		            [sg.T(you_won_txt, background_color=background_color, justification='center', font=('', 30))]
				 ]
			
		window = sg.Window('WRONG!!', layout, element_justification='center', no_titlebar=True, modal=True, keep_on_top=True, background_color=background_color, auto_close=True, finalize=True)

		while True:
			event, values = window.read()
			
			if event in (sg.WIN_CLOSED,):
				break
		window.close()

