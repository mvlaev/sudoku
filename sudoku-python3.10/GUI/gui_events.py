import PySimpleGUI as sg

from time import sleep

from GUI.gui_layouts import SudokuLayouts
from GUI.gui_logic   import SudokuLogic

class SudokuEvents:
	def __init__(self):
		self.logic  = SudokuLogic()
		self.window = self.logic.window
		
	
	def loop(self):
		
		
		while True:
			self.logic.update_all()
			
			event, values = self.window.read(timeout=1000)
			print(event)
			
			if event in (sg.WIN_CLOSED, sg.WINDOW_CLOSE_ATTEMPTED_EVENT, '-CLOSE-'):
				self.logic.close_window()
				break
			
			
			###Cases
			match event:
				case (i,j) if self.logic.timer.running:
							###clicked with mouse
							self.logic.mouse_click((i,j))
				case str():
					stripped = event.split(':')[0]
					
					if stripped in (map(str, list(range(1, self.logic.layouts.dim + 1)))):
						###Save the number
						self.logic.set_value(stripped)
					
					match stripped:
						case '__TIMEOUT__':
							self.logic.update_timer()
						case '-START-':
							self.logic.start_stop()
						case '-NEW-':
							self.logic.new_board()
							#self.logic.start_stop()
						case '-COLORS-' if self.logic.timer.running:
							self.logic.colors_on_off()
						case 'Up':
							self.logic.key_up()
						case 'Down':
							self.logic.key_down()
						case 'Left':
							self.logic.key_left()
						case 'Right':
							self.logic.key_right()
						case 'Delete'|'BackSpace':
							self.logic.delete_value()
						case 'c':
							self.logic.window['-COLORS-'].click()
						case 'n':
							self.logic.window['-NEW-'].click()
						case 's':
							self.logic.window['-START-'].click()
						

