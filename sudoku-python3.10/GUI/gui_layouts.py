import PySimpleGUI as sg

class SudokuLayouts:
	def __init__(self, lowest_dim = 3):
		
		self.low_dim = lowest_dim
		self.dim     = lowest_dim**2
		
		self.board_size = (494, 476)
		self.blanks_left_format = 'Blanks\nLeft: {}'
		
		self.layout_dict= self.small_frames_layout()
		self.board      = self.board_layout()
		self.tools      = self.tools_layout()
		self.top_layout = self.window_layout()
	
	def small_frames_layout(self):
		'''Small frames layout generator.
		
		Generates the layouts of the small frames as a dict of the form 
		{(small-frame-line, small-frame-row): [[sg.T,sg.T,sg.T ........], [sg.T,sg.T,sg.T........], [sg.T,sg.T,sg.T] .....]}
		'''
		
		small_frames_layout_dict = {}
		for k in range(self.dim):
			small_frames_layout_dict[k // self.low_dim, k % self.low_dim] = [
																	
												[sg.Text(
													
													  #text         = f'{(k//self.low_dim)*self.low_dim+i, (k%self.low_dim)*self.low_dim+j}',
													  text         = k,
													  size         = 2,
													  font         = ('', 26),
													  auto_size_text=True,
													  justification= 'center',
													  pad          = (1,1),
													  key          = ((k//self.low_dim)*self.low_dim+i, (k%self.low_dim)*self.low_dim+j),
													  enable_events= True,
													  expand_y     = False,
													  expand_x     = False,
													  relief       = 'solid',
													  border_width = 1,
	#												  use_readonly_for_disable = False,
	#												  readonly     = True,
													  metadata     = (k//self.low_dim, k%self.low_dim)
																				) for j in range(self.low_dim)
																			] for i in range(self.low_dim)
																		]
		return small_frames_layout_dict


	def board_layout(self):
		board_layout = [
							[sg.Frame('',
									  layout       = self.layout_dict[(i,j)],
									  pad          = (1,1),
									  relief       = 'sunken',
									  background_color='lightgrey',
									  border_width = 3,
									  
									  expand_y     = False,
									  expand_x     = True,
									  vertical_alignment='c',
									  element_justification='c'
									  ) for j in range(self.low_dim)
							] for i in range(self.low_dim)
						]
		return board_layout


	def tools_layout(self):
		tools_layout = [
					[sg.T('Elapsed:',font          = ('', 20),
									 justification = 'center',
									 expand_x      = True)
									 ],
					
					[sg.T('11:11:11', key='-TIMER-', font=('', 25), justification='c', expand_x=True)],
					
					[sg.T(self.blanks_left_format.format(''), key='BLANKS', justification= 'center'),
					 sg.ProgressBar(10, key='PROGRESS', expand_x=True, s=(180,10))
					 ],
					
					[sg.HorizontalSeparator()],
					
					[sg.Button('START',      key='-START-',  enable_events=True, border_width=1, expand_x=True, use_ttk_buttons=True,
					 mouseover_colors=('white', 'brown'),
					 button_color='lightgrey'
					 )],
					
					[sg.Button('New',        key='-NEW-',    enable_events=True, border_width=1, expand_x=True, use_ttk_buttons=True, mouseover_colors=('white', 'brown'))],
					
					[sg.HorizontalSeparator()],
					
					[sg.Button('Exit',       key='-CLOSE-',  enable_events=True, border_width=1, expand_x=True, use_ttk_buttons=True, mouseover_colors=('white', 'brown'))],
					
					[sg.Button('Turn Colors ON', key='-COLORS-', enable_events=True, border_width=1, expand_x=True, use_ttk_buttons=True,
					 button_color=('red','blue'),  mouseover_colors=('white', 'brown'))
					 ]
			]
		
		return tools_layout


	def window_layout(self):
		layout = [
					[sg.pin(sg.Frame('', self.board,
								  visible=False,
								  key='-BOARD-',
								  size=self.board_size,
								  expand_y = True,
								  border_width=10,
								  relief='ridge',
								  pad=0,
								  element_justification = "center",
								  vertical_alignment='c')),
					 sg.pin(sg.Frame('', self.tools, key='-TOOLS-', size=(200,500), visible=False))
					 ]
				  ]
		
		return layout
