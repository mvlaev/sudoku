from copy import deepcopy

class Board:
	'''Board container.
	
	Contains the sudoku game Board.
	0. Parameters:
			lowest_dim - the lowest dimension greater than 1, default = 3
			structure  - a predefined values for the board structure, default = None
	1. If no structure is passed, an empty one is generated (all zeroes).
	2. Attributes:
			dim         - the dimension of the Board
			lowest_dim  - the lowest dimension greater than 1
			last_error  - the last error ocurred
			__structure - where the Board structure is stored
	3. Methods:
			check_board(board)         - checks if a board is ok, True if OK, else False
			check_number(number, y, x) - checks if it is ok for a number to be inserted at position, True if OK, else - False
			get_available_values(y, x) - returns a list of the available choices at (y,x)
			get_blanks()			   - returns a list of the coordinates of the empty cells
			get_board()                - returns the current Board state as a list (of lists).
			get_board_as_dict()        - returns the board as dict of the form {(i,j) : __structure[i][j], .......}
			get_line(line_num)         - returns the line at position "line_num" of the Board as a list
			get_row(row_num)           - returns the row at position "row_num" of the Board as a list
			get_square(y, x)           - return the square that holds the element with coordinates y, x as a list of lists
			reset()                    - reset the Board, returns None
			set_item(number, y, x)     - set the value at position y, x to number
			
	'''
	
	def __init__(self, lowest_dim = 3, structure = None):
		self.dim         = lowest_dim**2
		self.lowest_dim  = lowest_dim
		self.last_error  = ''
		
#		self.__structure = [[x//lowest_dim + (y//lowest_dim)*lowest_dim for x in range(self.dim)] for y in range(self.dim)]
		if not structure:
			self.__structure = [[0 for x in range(self.dim)] for y in range(self.dim)]
		else:
			self.__structure = deepcopy(structure)
		
		self.__backup    = deepcopy(self.__structure)
		
	
	def check_number(self, number, y, x):
		if number in self.get_square(y, x) or number in self.get_line(y) or number in self.get_row(x):
			return False
		return True
	
	def check_board(self):
		allowed = set(range(self.dim + 1)) #Zeroes included
		
		###Check lines AND BAD SYMBOLS
		for line in self.get_board():
			if set(line).difference(allowed):
				return False
			if not len(line) - line.count(0) == len(set(line).difference({0})):
				return False
		
		###Check rows
		for i in range(self.dim):
			row = self.get_row(i)
			if not len(row)  - row.count(0) == len(set(row).difference({0})):
				return False
		
		###Check squares
		for i in range(self.lowest_dim):
			for j in range(self.lowest_dim):
				square = self.get_square(i*self.lowest_dim, j*self.lowest_dim)
				if not len(square) - square.count(0) == len(set(square).difference({0})):
					return False
		return True
	
	
	def get_line(self, line_num):
		if 0 <= line_num < self.dim:
#			print( self.__structure[line_num])
			return  self.__structure[line_num]
		self.last_error += f'Bad line num. Should be between 0 and {self.dim - 1}, got {line_num}\n'
		
	def get_row(self, row_num):
		if 0 <= row_num < self.dim:
#			print(list(list(zip(*self.__structure))[row_num]))
			return list(list(zip(*self.__structure))[row_num])
		self.last_error += f'Bad row num. Should be between 0 and {self.dim - 1}, got {row_num}\n'
		
	def get_square(self, y, x):
		if not (0 <= y < self.dim and 0 <= x < self.dim):
			self.last_error += f'Bad coordinates. X and Y should be between 0 and {self.dim - 1}, got ({x},{y})'
			return
		
		y_offset = y // self.lowest_dim
		x_offset = x // self.lowest_dim
		
#		print(f'''\
#x = [{self.lowest_dim*x_offset};{self.lowest_dim*(x_offset + 1)})
#y = [{self.lowest_dim*y_offset};{self.lowest_dim*(y_offset + 1)})''')
		return [
				self.__structure[y][x] for x in range(self.lowest_dim*x_offset, self.lowest_dim*(x_offset + 1), 1)\
									   for y in range(self.lowest_dim*y_offset, self.lowest_dim*(y_offset + 1), 1)
			   ]
	
	
	def get_available_values(self, y, x):
		
		#All possible nums:
		numbers = set(range(1, self.dim + 1))
		
		#Prepare the line and the row:
		line, row, square = set(self.get_line(y)), set(self.get_row(x)), set(self.get_square(y, x))
		
		available_numbers = numbers.difference(square).difference(line).difference(row)
		return list(available_numbers)
	
	def get_board(self):
		return self.__structure
	
	def get_board_as_dict(self):
		puzzle_as_list = self.get_board()
		puzzle_as_dict = {}
		
		for i in range(self.dim):
			for j in range(self.dim):
				puzzle_as_dict[(i,j)] = str(puzzle_as_list[i][j]) if puzzle_as_list[i][j] > 0 else ''
		
		return puzzle_as_dict
	
	def get_blanks(self):
		blanks = []
		for line in range(self.dim):
			for element in range(self.dim):
				if self.__structure[line][element] == 0:
					blanks.append((line, element))
		return blanks
	
#	@property
#	def structure(self):
#		return self.__structure
#	
#	@structure.setter
	def set_item(self, number, y, x):
		self.__structure[y][x] = number
		return
#		print(self.last_error)
#		self.last_error = ''
	
	def reset(self):
		self.__structure = deepcopy(self.__backup)
		
	def __str__(self):
		'''Ugly representation.
		
		'''
		
		to_return = '\n'.join([str(' '.join([str(el) for el in line])) for line in self.__structure])
		return to_return

if __name__ == '__main__':
	board = Board(structure = [
	[9, 5, 1, 2, 4, 6, 7, 8, 3],
	[8, 2, 7, 9, 5, 3, 6, 1, 4],
	[6, 4, 3, 8, 7, 1, 9, 5, 2],
	[2, 3, 8, 5, 9, 4, 1, 7, 6],
	[1, 6, 9, 3, 8, 7, 4, 2, 5],
	[4, 7, 5, 6, 1, 2, 8, 3, 9],
	[5, 9, 4, 1, 3, 8, 2, 6, 7],
	[7, 1, 6, 4, 2, 5, 3, 9, 8],
	[3, 8, 2, 7, 6, 9, 5, 4, 1],
	]
	)
	
	print(board.check_board())
