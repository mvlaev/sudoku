from random           import choice

from engine.sudoku_board     import Board
from engine.sudoku_solver    import SudokuSolve

class SudokuUnsolve:
	def __init__(self, board: Board, * , dim = 9):
		self.board  = board
		self.dim    = dim
		
		####Some backtrack info from the solver:
		self.max_backtrack = 0
#		self.points = deque((i, j) for j in range(dim) for i in range(dim))
	
	def delete_element(self):
		line, row  = choice(range(self.dim)), choice(range(self.dim))
#		line, row = choice(self.points)
#		initial_value = self.board._Board__structure[line][row]
		initial_value = self.board.get_board()[line][row]
#		initial_value = self.board.get_line(line)[row]
		if not initial_value == 0:
			self.board.set_item(0, line, row)
#			solver = SudokuSolve(Board(structure = self.board.get_board()))
#			status = solver.solve()
#			
#			###Some backtrack info
#			self.max_backtrack = max(solver.backtrack_lenght)
#			######################################################
#			
#			if status == 'UNSOLVEABLE':
#			
#				###TODO: DELETE ME!
#				print(f'{status}, returning....')
#				#####################################
#				
#				self.board.set_item(initial_value, line, row)
#				return
	
#	def delete_element(self):
#		y, x = choice(self.points)
#		initial_value = self.board.get_board()[y][x]
#		
#		self.board.set_item(0, y, x)
#		solver = SudokuSolve(Board(structure = self.board.get_board()))
#		status = solver.solve()
#		
#		if status == 'UNSOLVEABLE':
#			self.board.set_item(initial_value, y, x)
#			self.points.remove((y,x))
	
	def unsolve(self, blanks_count = 65):
		count = 0
		while len(self.board.get_blanks()) < blanks_count:
			self.delete_element()
			count += 1
		
#			print(count)

#################Check if the puzzle has solution
		solver = SudokuSolve(Board(structure = self.board.get_board()))
		status = solver.solve()
		
		###Some backtrack info
		self.max_backtrack = max(solver.backtrack_lenght)
		######################################################
		
		if status == 'UNSOLVEABLE':
		
			###TODO: DELETE ME!
			print(f'{status}, returning....')
			#####################################
			
			self.board.set_item(initial_value, line, row)
			print('error')
			return
##############################################################################

		print(self.max_backtrack)
		return self.board, self.max_backtrack
						

