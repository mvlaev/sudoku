# _Welcome to SudokuMV !_

## Introduction

SudokuMV consists of a Sudoku engine and a GUI. The goal of the project is to provide simple yet usable Sudoku engine written in pure Python and a GUI that exposes Sudoku gameplay.

You can use SudokuMV for mental exercise by solving some puzzles by yourself or you can use the functionality that the engine provides in your own project.

## Engine

SudokuMV's engine is a simple (brute force, naive...) engine. It relies heavily on the Python's built in random module.

Main features of the engine are:
- populate empty Sudoku board
- generate Sudoku puzzle with the desired number of blank positions
- solve existing puzzles
- check if a given solution (fully or partially populated board) has errors (by the Sudoku rules) in it
- check if a given number is present in the row, column and square where it is intended to be inserted


| Note: The design of the engine has the following specifics: |
| ----------------------------------------------------------- |
|1. The generated puzzle is NOT guaranteed to have only one solution. Actually it usually has more than one solution. |
|2. When solving a given puzzle with multiple possible solutions, the engine finds one of the solutions in a random manner, it is NOT designed to check if a puzzle has one and only one solution instead it just finds a solution if this exists at all.
|3. Although the engine uses multiprocessing to speed up the generating process, it will take some minutes if you try to generate a large (>1000) number of puzzles with many (>60) blanks.

## GUI

The GUI that comes with SudokuMV is designed to use the engine as a game engine. It does not expose all the features of the engine. For example you can not load your own puzzle in the GUI and solve it there...yet :) That being said, the GUI is functional and fun! It's in an early stage of development and was designed to serve as a showcase of the engine.

It takes advantage of the very useful [PySimpleGUI][sg] project.

Use:
- press New (or hit 'n' key) to generate new puzzle
- insert the number of blanks you want to solve, defaults to 31 (easy 5-10 minutes game)
- you can pause and resume the game by clicking on the Start/Stop button or pressing the 's' key
- you can turn on/off auxiliary coloring by clicking the Turn Colors On/Off button or pressing 'c' key

## Install

There are 2 folders that contain the same project sorce code with the exception that the sudoku-python3.10 has some 'match' constructions that came out with the python3.10. The code in sudoku-python3 was tested with python3.8 but it should be able to run on python3.7 too. Choose the one that corresponds to your python3 version.

The GUI depends on the [PySimpleGUI][sg] package, so you need to install it if you intend to use the GUI.
```
pip install PySimpleGUI
```
If you are on Linux, you may need to install Tkinter too. Use your distribution's package management tool to do that.

The engine does not depend on external packages.

## Run
To run SudokuMV with the GUI use the entry point ```main.py```.

```
python3 /path/to/main.py
```


[sg]: <https://pysimplegui.readthedocs.io/en/latest/>
